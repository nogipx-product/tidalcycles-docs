import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:clean_architecture/clean_architecture.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/src/logger.dart';
import 'package:tidalcyclesdocs/data/tidal_functions.dart';
import 'package:tidalcyclesdocs/router/router.dart';
import 'package:tidalcyclesdocs/router/router.gr.dart';
import 'package:tidalcyclesdocs/screen/full_description.dart';
import 'package:tidalcyclesdocs/widget/colors.dart';
import 'package:tidalcyclesdocs/widget/drawers.dart';

void main() async{
  TidalFunctions.patternTransformers
    .map((e) => e.toJson())
    .forEach((element) => print(
      jsonEncode(element)
        .replaceAll(RegExp(r'[ ]{2,}'), ' ')
        .replaceAll(r'\n', '')
        + "\n\n"
      )
    );

//  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TidalCycles Docs',
      theme: ThemeData(
        primaryColor: TidalColor.tidal_cyan,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: GoogleFonts.ptSansTextTheme().apply(
          bodyColor: Colors.black,
          displayColor: Colors.black
        ),
        accentTextTheme: GoogleFonts.oswaldTextTheme().apply(
          bodyColor: Colors.black,
          displayColor: Colors.black
        )
      ),
      builder: (context, child) {
        return CleanApplication(
          configuration: TidalDocsConfig(
            child: ExtendedNavigator<MobileRouter>(router: MobileRouter()),
          )
        );
      },
    );
  }
}

class TidalDocsConfig
  extends InheritedWidget
  implements Configuration {
  final Widget child;

  TidalDocsConfig({this.child}) : super(child: child);

  @override
  List<Factory<Controller>> controllers() {
    return [];
  }

  @override
  List<Implementation<Repository>> repositories() {
    return [];
  }

  @override
  List<Factory<Service>> services() {
    return [
      Factory<Drawers>(factory: () => Drawers())
    ];
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  @override
  List<UseCase> usecases() {
    return [];
  }

  @override
  Logger get log => throw UnimplementedError();

  @override
  bool wrapViewWithSafeArea() => false;
  
}