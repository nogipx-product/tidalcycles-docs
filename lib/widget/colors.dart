import 'dart:ui';

class TidalColor {
  static const jet = Color(0xff313131);

  static const tidal_cyan = Color(0xff57c6c6);
  static const tidal_purple = Color(0xffc11c5f);
  static const tidal_blue = Color(0xff18274e);
}