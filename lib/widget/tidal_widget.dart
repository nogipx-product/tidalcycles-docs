import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tidalcyclesdocs/data/tidal_domain.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:tidalcyclesdocs/router/router.gr.dart';
import 'package:loggable_mixin/loggable_mixin.dart';

class GenerateDocContent extends StatelessWidget with Loggable {
  final TidalFunc function;
  final Widget Function(BuildContext, Snippet) snippetBuilder;
  final Widget errorSnippet;
  final TextStyle textStyle;
  final EdgeInsets textPadding;

  GenerateDocContent({Key key,
    @required this.function,
    @required this.snippetBuilder,
    this.errorSnippet,
    this.textStyle,
    this.textPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final funText = function.splitBySnippet;
    return Container(
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: funText.length,
        itemBuilder: (context, index) {
          final textPiece = funText[index];

          if (TidalFunc.snippetRegExp.hasMatch(textPiece)) {

            final snippetIndexRaw = TidalFunc.snippetRegExp
              .firstMatch(textPiece)
              .group(1);
            final snippetIndex = int.tryParse(snippetIndexRaw) - 1;

            try {
              return snippetBuilder(context, Snippet(function.snippets[snippetIndex]));

            } catch(e) {
              return errorSnippet ?? Container(
                padding: EdgeInsets.all(16),
                color: Colors.redAccent.hover,
                child: Text("Error while getting snippet"),
              );
            }

          } else return Padding(
            padding: textPadding ?? EdgeInsets.symmetric(
              horizontal: 16, vertical: 8
            ),
            child: Text(textPiece.trim(),
              style: textStyle ?? Theme.of(context).textTheme.subtitle1,
            ),
          );
        }
      ),
    );
  }
}

class TidalFunctionListItem extends StatelessWidget {

  final Color color;
  final TidalFunc function;

  const TidalFunctionListItem({Key key,
    this.color,
    this.function
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        hoverColor: color.hover,
        splashColor: color.pressed,
        focusColor: color.activated,
        highlightColor: color.selected,
        onTap: () {
          MobileRouter.navigator.pop();
          MobileRouter.navigator.pushTidalFunctionDescriptionScreen(
            tidalFunc: function
          );
        },
        child: Container(
          decoration: BoxDecoration(
            color: color.hover
          ),
          height: 50,
          child: Row(
            children: [
              Container(
                width: 4,
                color: color ?? Theme.of(context).primaryColor,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(function.name ?? "No name function",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}