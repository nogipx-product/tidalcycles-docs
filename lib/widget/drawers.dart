import 'package:clean_architecture/clean_architecture.dart';
import 'package:flutter/material.dart';
import 'package:tidalcyclesdocs/widget/colors.dart';
import 'package:tidalcyclesdocs/data/tidal_domain.dart';
import 'package:tidalcyclesdocs/data/tidal_functions.dart';
import 'package:tidalcyclesdocs/widget/tidal_widget.dart';

class Drawers extends Service {

  Widget functionsList(context) => Container(
    width: MediaQuery.of(context).size.width * .6,
    child: Drawer(
      child: SingleChildScrollView(
        padding: EdgeInsets.only(top: 50),
        child: Column(
          children: [
            _listSection(context,
              title: "Pattern Transformers",
              color: Colors.green,
              functions: TidalFunctions.patternTransformers
            )
          ],
        ),
      )
    ),
  );

  static Widget _listSection(context, {String title, Color color, List<TidalFunc> functions}) {
    assert(functions != null);
    // TODO add slivers sticky headers
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null && title.isNotEmpty)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(title,
              style: Theme.of(context).textTheme.headline6.apply(
                color: TidalColor.jet,
                fontWeightDelta: 800
              )
            ),
          ),
        ListView.separated(
          padding: EdgeInsets.only(top: 8),
          shrinkWrap: true,
          primary: false,
          itemCount: functions.length,
          itemBuilder: (_, index) {
            final func = functions[index];
            final finalColor = func.text.trim().isEmpty
              ? Colors.black26
              : color;
            return TidalFunctionListItem(
              color: finalColor ?? Colors.blueGrey,
              function: func,
            );
          },
          separatorBuilder: (_, __) => Divider(
            height: 8,
            thickness: 1,
            color: Colors.transparent,
          ) ,
        ),
      ],
    );

  }

}