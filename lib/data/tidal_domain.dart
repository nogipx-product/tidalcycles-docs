import 'package:json_annotation/json_annotation.dart';

part 'tidal_domain.g.dart';

@JsonSerializable(
  explicitToJson: true,
)
class TidalFunc {
  final String name;
  final List<String> snippets;
  final String text;

  TidalFunc({
    this.name,
    this.snippets,
    this.text
  });

  static RegExp get snippetRegExp => RegExp(r'@snippet\(([-]{0,1}\d+)\)');

  List<String> get splitBySnippet {
    if (text == null)
      return [];

    final divider = '|';
    final examples = snippetRegExp
      .allMatches(text)
      .toSet()
      .map((e) => e.group(0));

    String wrappedText = text;
    examples.forEach((e) {
      wrappedText = wrappedText.replaceAll(e, "$divider$e$divider");
    });

    return wrappedText
      .replaceAll(RegExp(r'[ ]{2,}'), '')
      .split(divider)
      .map((e) => e.trim())
      .where((e) => e.isNotEmpty)
      .toList();
  }

  factory TidalFunc.fromJson(json) => _$TidalFuncFromJson(json);
  toJson() => _$TidalFuncToJson(this);
}

class Snippet {
  final String _value;

  Snippet(this._value);

  @override
  String toString() {
    return _value
      .replaceAll(RegExp(r'[ ]{2,}'), '')
      .replaceAll(r'$ ', '\n  \$ ')
      .replaceAll(r'# ', '\n  # ');
  }

  String tab(int spaces, String text) {
    return " "*spaces + text;
  }

  String t0(String text) => tab(0, text);
  String t2(String text) => tab(2, text);
  String t4(String text) => tab(4, text);
  String t6(String text) => tab(6, text);
  String t8(String text) => tab(8, text);
  String t10(String text) => tab(10, text);
  String t12(String text) => tab(12, text);
}