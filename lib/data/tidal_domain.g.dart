// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tidal_domain.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TidalFunc _$TidalFuncFromJson(Map<String, dynamic> json) {
  return TidalFunc(
    name: json['name'] as String,
    snippets: (json['snippets'] as List)?.map((e) => e as String)?.toList(),
    text: json['text'] as String,
  );
}

Map<String, dynamic> _$TidalFuncToJson(TidalFunc instance) => <String, dynamic>{
      'name': instance.name,
      'snippets': instance.snippets,
      'text': instance.text,
    };
