import 'package:tidalcyclesdocs/data/tidal_domain.dart';

class TidalFunctions {

  static final patternTransformers = [
    TidalFunc(
      name: "Beat rotation",
      snippets: [
        r'(<~) :: Pattern Time -> Pattern a -> Pattern a',
        r'(~>) :: Pattern Time -> Pattern a -> Pattern a',
        r'd1 $ every 4 (0.25 <~) $ sound ("arpy arpy:1 arpy:2 arpy:3")',
        r'd1 $ every 4 (0.25 ~>) $ sound ("bd ~ sn:1 [mt ht]")',
        r'd1 $ "<0 0.5 0.125>" <~ sound ("arpy arpy:1 arpy:2 arpy:3")',
      ],
      text: """
        @snippet(1)
        or
        @snippet(2)
        (The above means that <~ and ~> are functions that are given a time pattern and a pattern of any type, and returns a pattern of the same type.)
        Shifts a pattern either forward or backward in time.
        
        For example, to shift a pattern by a quarter of a cycle, every fourth cycle:
        @snippet(3)
        @snippet(4)
        Or to alternate between different shifts:
        @snippet(5)
      """
    ),
    TidalFunc(
      name: "Mapping over patterns",
      snippets: [
        r'd1 $ sound "bd*2 [bd [sn sn*2 sn] sn]" # speed ((*2) <$> sine)',
        r'd1 $ sound "bd*2 [bd [sn sn*2 sn] sn]" # speed (sine*2)',
        r'd1 $ sound "bd*2 [bd [sn sn*2 sn] sn]" # speed (2*sine)',
      ],
      text: """
        Sometimes you want to transform all the events inside a pattern, and not the time structure of the pattern itself. 
        For example, if you wanted to pass a sinewave to speed, but wanted the sinewave to go from 0 to 2 rather than from 0 to 1, you could do this:
        @snippet(1)
        The above applies the function (* 2) (which simply means multiply by two), to all the values inside the sine pattern.
        
        However since Tidal 0.9, with patterns of numbers you can do arithmetic directly, like this:
        @snippet(2)
        There is a gotcha here in that you’ll want to arrange things so the pattern you’re working on is on the left hand side of any arithmetic.
        For example, this doesn’t work well:
        @snippet(3)
        This is because of the rule in Tidal that “the structure comes from the left”, 
        in this example the structure of the sine wave is lost to the structure of the number 2.
      """
    ),
    TidalFunc(
      name: "brak",
      snippets: [
        r'brak :: Pattern a -> Pattern a',
        r'd1 $ brak $ sound "[feel feel:3, hc:3 hc:2 hc:4 ho:1]"'
      ],
      text: """
        @snippet(1)
        (The above means that brak is a function from patterns of any type, to a pattern of the same type.)

        Make a pattern sound a bit like a breakbeat. 
        It does this by every other cycle, squashing the pattern to fit half a cycle, and offsetting it by a quarter of a cycle.
        @snippet(2)
      """
    ),
    TidalFunc(
      name: "degrade",
      snippets: [
        r'degrade :: Pattern a -> Pattern a',
        r'd1 $ slow 2 $ degrade $ sound "[[[feel:5*8,feel*3] feel:3*8], feel*4]" # accelerate "-6" # speed "2"',
        r'd1 $ slow 2 $ sound "bd ~ sn bd ~ bd? [sn bd?] ~"',
        r'd1 $ slow 2 $ sound "[[[feel:5*8,feel*3] feel:3*8]?, feel*4]"'
      ],
      text: """
        @snippet(1)
        degrade randomly removes events from a pattern 50% of the time:
        @snippet(2)
        The shorthand syntax for degrade is a question mark: ?. Using ? will allow you to randomly remove events from a portion of a pattern:
        @snippet(3)
        You can also use ? to randomly remove events from entire sub-patterns:
        @snippet(4)
      """
    ),
    TidalFunc(
      name: "degradeBy",
      snippets: [
        r'degradeBy :: Double -> Pattern a -> Pattern a',
        r'd1 $ slow 2 $ degradeBy 0.9 $ sound "[[[feel:5*8,feel*3] feel:3*8], feel*4]" # accelerate "-6" # speed "2"',
        r'd1 $ slow 2 $ sound "bd ~ sn bd ~ bd? [sn bd?] ~"',
        r'd1 $ slow 2 $ sound "[[[feel:5*8,feel*3] feel:3*8]?, feel*4]"'
      ],
      text: """
        @snippet(1)
        Similar to degrade degradeBy allows you to control the percentage of events that are removed. 
        For example, to remove events 90% of the time:
        @snippet(2)
      """
    ),
    TidalFunc(
      name: "fast",
      snippets: [
        r'fast :: Pattern Time -> Pattern a -> Pattern a',
        r'd1 $ sound (fast 2 "bd sn kurt") # fast 3 (vowel "a e o")'
      ],
      text: """
        @snippet(1)
        Speed up a pattern. For example, the following will play the sound pattern "bd sn kurt" twice as fast (i.e. so it repeats twice per cycle), 
        and the vowel pattern three times as fast:
        @snippet(2)
        You can also use this function by its older alias, density.
      """
    ),
    TidalFunc(
      name: "fit",
      snippets: [
        r'fit :: Int -> [a] -> Pattern Int -> Pattern a',
        r'd1 $ sound (fit 3 ["bd", "sn", "arpy", "arpy:1", "casio"] "0 [~ 1] 2 1")',
        r"fit' :: Time -> Int -> Pattern Int -> Pattern Int -> Pattern a -> Pattern a",
        r'''d1 $ sound (fit' 1 2 "0 1" "1 0" "bd sn")'''
        r'''d1 $ fit' 1 4 (run 4) "[0 3*2 2 1 0 3*2 2 [1*8 ~]]/2" $ chop 4 $ (sound "breaks152" # unit "c")'''
      ],
      text: """
      """
    ),
    TidalFunc(
      name: "iter",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "jux (and juxBy)",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "linger",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "palindrome",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "rev",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "scramble",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "shuffle",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "slow",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "smash",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "spread",
      snippets: [
        r'd1 $ sound "ho ho:2 ho:3 hc"',
        r'spread :: (a -> t -> Pattern b) -> [a] -> t -> Pattern b'
      ],
      text: """
        @snippet(1)
        (The above is difficult to describe, if you don’t understand Haskell, just ignore it and read the below..)
        
        The spread function allows you to take a pattern transformation which takes a parameter, 
        such as slow, and provide several parameters which are switched between. 
        In other words it ‘spreads’ a function across several values.
         
        Taking a simple high hat loop as an example:
        @snippet(2)
        We can slow it down by different amounts, such as by a half:
        @snippet(1)
      """
    ),
    TidalFunc(
      name: "toScale",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "trunc",
      snippets: [

      ],
      text: """
      """
    ),
    TidalFunc(
      name: "zoom",
      snippets: [

      ],
      text: """
      """
    ),
  ];
}