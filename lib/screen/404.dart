import 'package:clean_architecture/clean_architecture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tidalcyclesdocs/widget/drawers.dart';

class Screen404 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: CleanApplication.service<Drawers>().functionsList(context),
      body: Center(
        child: Text("Что-то пошло не так..."),
      ),
    );
  }
}