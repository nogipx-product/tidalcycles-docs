import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/themes/atom-one-dark-reasonable.dart';
import 'package:loggable_mixin/loggable_mixin.dart';
import 'package:tidalcyclesdocs/router/router.gr.dart';
import 'package:tidalcyclesdocs/widget/drawers.dart';
import 'package:tidalcyclesdocs/data/tidal_domain.dart';
import 'package:tidalcyclesdocs/widget/tidal_widget.dart';

class TidalFunctionDescriptionScreen extends StatelessWidget with Loggable {

  final TidalFunc tidalFunc;

  TidalFunctionDescriptionScreen({Key key, this.tidalFunc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final drawers = Drawers();

    if (tidalFunc == null)
      MobileRouter.navigator.pushNamedAndRemoveUntil(Routes.screen404,
          (route) => route.settings.name == Routes.screen404
      );

    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(),
      endDrawer: drawers.functionsList(context),
      body: tidalFunc != null
        ? SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 16, horizontal: 12
                ),
                child: Text(tidalFunc.name,
                  textAlign: TextAlign.start,
                  style: Theme.of(context).accentTextTheme.headline3.apply(
                    fontWeightDelta: 500,
                    color: Colors.black
                  ),
                ),
              ),
              GenerateDocContent(
                function: tidalFunc,
                textStyle: Theme.of(context).textTheme.bodyText1.apply(),
                snippetBuilder: (context, snippet) => _buildSnippet(context, snippet),
              )
            ],
          ),
        )
        : throw "Cannot show description. Function not specified.",
    );
  }

  _buildSnippet(BuildContext context, Snippet snippet) {
    final theme = atomOneDarkReasonableTheme;
    return Container(
      color: theme["root"].backgroundColor,
      padding: EdgeInsets.symmetric(
        vertical: 12, horizontal: 16
      ),
      child: HighlightView(snippet.toString(),
        theme: theme,
        language: "haskell",
      ),
    );
  }
}