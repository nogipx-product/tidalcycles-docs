
import 'package:auto_route/auto_route_annotations.dart';
import 'package:tidalcyclesdocs/screen/404.dart';
import 'package:tidalcyclesdocs/screen/full_description.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true
)
class $MobileRouter {

  @initial
  TidalFunctionDescriptionScreen tidalFunctionDescriptionScreen;

  Screen404 screen404;
}