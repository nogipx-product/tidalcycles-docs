// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:tidalcyclesdocs/screen/full_description.dart';
import 'package:tidalcyclesdocs/data/tidal_domain.dart';
import 'package:tidalcyclesdocs/screen/404.dart';

abstract class Routes {
  static const tidalFunctionDescriptionScreen = '/';
  static const screen404 = '/screen404';
}

class MobileRouter extends RouterBase {
  //This will probably be removed in future versions
  //you should call ExtendedNavigator.ofRouter<Router>() directly
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<MobileRouter>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.tidalFunctionDescriptionScreen:
        if (hasInvalidArgs<TidalFunctionDescriptionScreenArguments>(args)) {
          return misTypedArgsRoute<TidalFunctionDescriptionScreenArguments>(
              args);
        }
        final typedArgs = args as TidalFunctionDescriptionScreenArguments ??
            TidalFunctionDescriptionScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => TidalFunctionDescriptionScreen(
              key: typedArgs.key, tidalFunc: typedArgs.tidalFunc),
          settings: settings,
        );
      case Routes.screen404:
        return MaterialPageRoute<dynamic>(
          builder: (_) => Screen404(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//TidalFunctionDescriptionScreen arguments holder class
class TidalFunctionDescriptionScreenArguments {
  final Key key;
  final TidalFunc tidalFunc;
  TidalFunctionDescriptionScreenArguments({this.key, this.tidalFunc});
}

//**************************************************************************
// Navigation helper methods extension
//***************************************************************************

extension MobileRouterNavigationHelperMethods on ExtendedNavigatorState {
  Future pushTidalFunctionDescriptionScreen({
    Key key,
    TidalFunc tidalFunc,
  }) =>
      pushNamed(Routes.tidalFunctionDescriptionScreen,
          arguments: TidalFunctionDescriptionScreenArguments(
              key: key, tidalFunc: tidalFunc));
  Future pushScreen404() => pushNamed(Routes.screen404);
}
